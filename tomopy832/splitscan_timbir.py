from __future__ import print_function
import h5py
import sys


#python extract_timbir.py filename [start end]

#Currently the script just outputs every block of sz projections (currently hard coded below based on sz1 and sz2, the number of projections in the first and second 180 degrees, which is generally different by 1 for our timbir scans) into a separate file (filename_0001.h5 etcetera). 'start' and 'end' are optional, and specify which blocks of 180 degrees to extract (it takes a while to extract everything).

#One issue is with the flat fields: to make it readable by dxchange and current scripts, this script pretends that the flat fields are taken just before and just after the block was acquired. In practice, the flat fields were taken much earlier and later than this (before and after the full timbir scan), so this can probable be improved, but may require changes in dxchange or our other scripts...

#for dkshuh on 3/30/17, last angle number is 15329

#fn is file name
fn = sys.argv[1]
#sz is number of projections per 180, should be 63 for that scan
#alternates 255 and 256 for this timbir scan (interval is 0.703125, on first half first angle is 0.703125, last angle is 179.296875, so it's not exactly 180 degrees, on second half it's 180.3515625 through 359.6484375)
sz1 = 31
sz2 = 32
sz = sz1+sz2

f = h5py.File(fn,'r')

bs = list(f.keys())[0]

d = f[bs]
fl = list(d.keys())

drks = sorted([g for g in fl if 'drk' in g])
baks = sorted([g for g in fl if 'bak' in g])

prjs = [g for g in fl if not 'bak' in g and not 'drk' in g]
nout = int(len(prjs)/sz)

if len(sys.argv)>3:
    start = int(sys.argv[2])
    end = int(sys.argv[3])
else:
    start = 0
    end = nout

	
for i in range(start,end):
    bsout = bs+'_{:04d}'.format(i+1)
    print(bsout + ' ({}/{})'.format(i+1,nout))
    fout = h5py.File(fn[:-3] +'_{:04d}.h5'.format(i+1), 'w')
    dout = fout.create_group(bsout)
    for a in list(d.attrs):
        dout.attrs[a] = d.attrs[a]
    for a in list(f.attrs):
        fout.attrs[a] = f.attrs[a]
 #   dout.attrs['nangles'] = '{}'.format(sz)
    dout.attrs['nangles'] = '{}'.format(sz1)
    dout.attrs['arange'] = '{}'.format(float(d[bs+'_0000_{:04d}.tif'.format(i*sz+sz1-1)].attrs['rot_angle'])-float(d[bs+'_0000_{:04d}.tif'.format(i*sz)].attrs['rot_angle']))
    for j in range(sz1):
        dt = d[bs+'_0000_{:04d}.tif'.format(i*sz+j)]
        im = dt[:]
        dout.create_dataset(bsout+'_0000_{:04d}.tif'.format(j), data=im)
        dtout = dout[bsout+'_0000_{:04d}.tif'.format(j)]
        for a in list(dt.attrs):
            dtout.attrs[a] = dt.attrs[a]
    for j in range(len(drks)):
        im = d[drks[j]][:]
        dout.create_dataset(bsout+'drk_{:04d}_{:04d}.tif'.format(j,sz1-1), data=im)
    for j in range(len(baks)):
        im = d[baks[j]][:]
        t = 0
        if j%2==1:
            t = sz1-1
        dout.create_dataset(bsout+'bak_{:04d}_{:04d}.tif'.format(int(j/2),t), data=im)
		
    bsouta = bs+'a_{:04d}'.format(i+1)
    print(bsouta + ' ({}/{})'.format(i+1,nout))
    fouta = h5py.File(fn[:-3] +'a_{:04d}.h5'.format(i+1), 'w')
    douta = fouta.create_group(bsouta)
    for a in list(d.attrs):
        douta.attrs[a] = d.attrs[a]
    douta.attrs['nangles'] = '{}'.format(sz2)
    douta.attrs['arange'] = '{}'.format(float(d[bs+'_0000_{:04d}.tif'.format(i*sz+sz2-1)].attrs['rot_angle'])-float(d[bs+'_0000_{:04d}.tif'.format(i*sz)].attrs['rot_angle']))
    for j in range(sz2):
        dt = d[bs+'_0000_{:04d}.tif'.format(i*sz+sz1+j)]
        im = dt[:]
        douta.create_dataset(bsouta+'_0000_{:04d}.tif'.format(j), data=im)
        dtouta = douta[bsouta+'_0000_{:04d}.tif'.format(j)]
        for a in list(dt.attrs):
            dtouta.attrs[a] = dt.attrs[a]
    for j in range(len(drks)):
        im = d[drks[j]][:]
        douta.create_dataset(bsouta+'drk_{:04d}_{:04d}.tif'.format(j,sz2-1), data=im)
    for j in range(len(baks)):
        im = d[baks[j]][:]
        t = 0
        if j%2==1:
            t = sz2-1
        douta.create_dataset(bsouta+'bak_{:04d}_{:04d}.tif'.format(int(j/2),t), data=im)		
		
		
		
    fout.close()