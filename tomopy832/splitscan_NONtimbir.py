from __future__ import print_function
import h5py
import sys


#python extract_timbir.py filename number_of_projections_per_180 [start end]

#Currently the script just outputs every block of number_of_projections_per_180 projections into a separate file (filename_0001.h5 etcetera). 'start' and 'end' are optional, and specify which blocks of 180 degrees to extract (it takes a while to extract everything).

#One issue is with the flat fields: to make it readable by dxchange and current scripts, this script pretends that the flat fields are taken just before and just after the block was acquired. In practice, the flat fields were taken much earlier and later than this (before and after the full timbir scan), so this can probable be improved, but may require changes in dxchange or our other scripts...

#for dkshuh on 3/30/17, last angle number is 15329

#in general, if user set 257 as nangles per 180 degrees, sz should be 256 (one less), to avoid overlap of images. this also means that arange is updated to have one increment less than 180 degrees.


#fn is file name
fn = sys.argv[1]
#sz is number of projections per 180,

sz = int(sys.argv[2])

f = h5py.File(fn,'r')

bs = list(f.keys())[0]

d = f[bs]
fl = list(d.keys())

drks = sorted([g for g in fl if 'drk' in g])
baks = sorted([g for g in fl if 'bak' in g])

prjs = [g for g in fl if not 'bak' in g and not 'drk' in g]
nout = int(len(prjs)/sz)

if len(sys.argv)>3:
    start = int(sys.argv[3])
    end = int(sys.argv[4])
else:
    start = 0
    end = nout
	
arange = 180.0-180.0/sz
	
for i in range(start,end):
    bsout = bs+'_{:04d}'.format(i+1)
    print(bsout + ' ({}/{})'.format(i+1,nout))
    fout = h5py.File(fn[:-3] +'_{:04d}.h5'.format(i+1), 'w')
    dout = fout.create_group(bsout)
    for a in list(d.attrs):
        dout.attrs[a] = d.attrs[a]
    for a in list(f.attrs):
        fout.attrs[a] = f.attrs[a]
    dout.attrs['nangles'] = '{}'.format(sz)
    dout.attrs['arange'] = '{}'.format(arange)
    for j in range(sz):
        dt = d[bs+'_0000_{:04d}.tif'.format(i*sz+j)]
        im = dt[:]
        dout.create_dataset(bsout+'_0000_{:04d}.tif'.format(j), data=im)
        dtout = dout[bsout+'_0000_{:04d}.tif'.format(j)]
        for a in list(dt.attrs):
            dtout.attrs[a] = dt.attrs[a]
    for j in range(len(drks)):
        im = d[drks[j]][:]
        dout.create_dataset(bsout+'drk_{:04d}_{:04d}.tif'.format(j,sz-1), data=im)
    for j in range(len(baks)):
        im = d[baks[j]][:]
        t = 0
        if j%2==1:
            t = sz-1
        dout.create_dataset(bsout+'bak_{:04d}_{:04d}.tif'.format(int(j/2),t), data=im)
		
		
    fout.close()