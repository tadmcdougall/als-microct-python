TomoPy installation instructions
================================

Linux
-----

### Using conda

The following can be used to install/update TomoPy and its dependencies in a
conda environment:

    conda update -c defaults -c als832 -c conda-forge --all
    conda install -c defaults -c als832 -c conda-forge tomopy

This uses the conda package in the 'als832' channel, and takes dependencies
from the 'conda-forge' channel.
