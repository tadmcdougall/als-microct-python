# once you have your h5 file (output of this script), run:
# python bl832_heartbeat.py -i your_file.h5
# And you will have the spot h5 format.

import h5py
import numpy
from PIL import Image 
import logging


input_file = 'oneMicronSource360h5.h5'
input_group = 'oneMicronSource360h5'
output_file = 'projections832_odd360_cropOffset.h5'
output_group = 'projections832_odd360_cropOffset'
output_dataset = 'projections832_odd360_cropOffset'

im = Image.open('projections832_odd360_cropOffset.tif') 
tomo = numpy.zeros((513,64,300)) #angle, y, x
for i in range(im.n_frames):
    im.seek(i)
    tomo[i] = numpy.array(im.getdata()).reshape(64,300)

with h5py.File(input_file, 'r') as g:
    file_data = dict(g.attrs)
    group_data = dict(g[input_group].attrs)
	
with h5py.File(output_file, 'w') as f:
	f.create_group(output_group)
	f[output_group].create_dataset(output_dataset, data=tomo)
	for key, val in file_data.iteritems():
		f.attrs.create(key, val)
	for key, val in group_data.iteritems():
		f[output_group].attrs.create(key, val)

