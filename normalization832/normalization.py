import h5py
import numpy as np
import argparse


def gen_histo(datafile, filename='default'):

    """Reads and h5 tomography file and generates a histogram for the projections, brights, and darks. The histrogram
       values and bins are saved for each class.

            Parameters
            ----------
            datafile : str
                Name of h5 file to read datastes from.
            filename : str
                Filepath to save histogram numpy arrays to.

            Returns
            ------
            data_projs : numpy array
                Stacked array of all projection values.
            data_brights : numpy array
                Stacked array of all bright values.
            data_darks : numpy array
                Stacked array of all dark values.

    """

    # read h5 file
    f = h5py.File(datafile, 'r')

    # root keys
    groups = f.keys()

    # datasets in first group
    if datafile[-10:-8] == '_x' and datafile[-6] == 'y':
        g = f[groups[0]]
        sub_groups = g.keys()
        datasets = g[sub_groups[0]]

    else:
        datasets = f[groups[0]]

    # placeholders for projection, brights, and darks
    data_projs = 0
    data_brights = 0
    data_darks = 0

    # count of projections, brights, and darks
    projs = 0
    brights = 0
    darks = 0

    # stack numpy arrays for projections, brights, and darks separately.
    for name in datasets:

        # print(name[-17:-14])
        data = datasets[name][:]

        if name[-17:-14] == 'bak':
            # print(name)
            if brights == 0:
                data_brights = data
            else:
                data_brights = np.vstack((data_brights, data))

            brights += 1

        elif name[-17:-14] == 'drk':
            # print(name)
            if darks == 0:
                data_darks = data
            else:
                data_darks = np.vstack((data_darks, data))

            darks += 1

        else:
            # print(name)
            if projs == 0:
                data_projs = data
            else:
                data_projs = np.vstack((data_projs, data))

            projs += 1

    if filename == 'default':
        filename = datafile[:-3]

    # cut of '.h5' from filename if it is there
    if filename[-3:] == '.h5':
        filename = filename[:-3]

    # generate and save histograms for all three classes (only if each class exists within the h5 file)
    if projs > 0:
        projs_hist, projs_edges = np.histogram(data_projs, bins=256)
        np.save(filename + "_projs_hist", projs_hist)
        np.save(filename + "_projs_edges", projs_edges)

    if brights > 0:
        brights_hist, brights_edges = np.histogram(data_brights, bins=256)
        np.save(filename + "_brights_hist", brights_hist)
        np.save(filename + "_brights_edges", brights_edges)

    if darks > 0:
        darks_hist, darks_edges = np.histogram(data_darks, bins=256)
        np.save(filename + "_darks_hist", darks_hist)
        np.save(filename + "_darks_edges", darks_edges)

    return data_projs, data_brights, data_darks


def scale_min_max(percent, data):

    """Calculates min and max after removal of outliers.

            Parameters
            ----------
            percent : int
                Percentage of outliers that should be removed on each end of the distribution.
            data : numpy array
                Stacked array of all projection values.

            Returns
            ------
            min : unit16
                Minimum value of projection datasets after outlier removal.
            max :unit16
                Maximum value of projection datasets after outlier removal.

    """

    # flatten array of all projection values
    flat_projs = data.flatten()

    # sort array of all projection values
    sort_projs = np.sort(flat_projs, axis=None)

    # number of projection values
    pixels = len(sort_projs)

    # scaled index of new min and max values
    min_index = int((float(percent)/100) * pixels)
    max_index = int(((100-float(percent))/100) * pixels)

    # min and max values
    mini = sort_projs[min_index]
    maxa = sort_projs[max_index]

    return mini, maxa


def norma():

    """Generates histograms for a given h5 tomography file and calculates min and max after removal of outliers.

            Parameters
            ----------
            percent : int
                Percentage of outliers that should be removed on each end of the distribution.
            datafile : str
                Name of h5 file to read datasets from.
            filename : str
                File path to save histogram numpy arrays to.

            Returns
            ------
            min_max: list
                A list of lists containing the scaled min and max for each class (projections, brights, darks).


    """
    # read and parse system arguments
    parser = argparse.ArgumentParser(description='Generate histograms from HDF5 file and calculate scaled min/max.')

    parser.add_argument('-p', '--percent', type=float, help='Percentage of outliers that should be removed on each end '
                                                            'of the distribution.', default=1)

    parser.add_argument('-d', '--datafile', required=True, type=str, help='Name of h5 file to read datasets from.',)

    parser.add_argument('-f', '--filename', type=str, help='File path to save histogram numpy arrays to.',
                        default='default')

    args = parser.parse_args()

    # generate histograms
    data_projs, data_brights, data_darks = gen_histo(args.datafile, args.filename)

    # type count
    count = 0

    # list of min/max for each class
    min_max = []

    # find scaled min, max for each type of dataset (if applicable)
    if type(data_projs) != int:
        mini_projs, maxa_projs = scale_min_max(args.percent, data_projs)
        min_max.append([mini_projs, maxa_projs])

    if type(data_brights) != int:
        mini_brights, maxa_brights = scale_min_max(args.percent, data_brights)
        min_max.append([mini_brights, maxa_brights])

    if type(data_darks) != int:
        mini_darks, maxa_darks = scale_min_max(args.percent, data_darks)
        min_max.append([mini_darks, maxa_darks])

    return min_max

"""
def main():
    min_max = norma()
    print(min_max)
main()
"""